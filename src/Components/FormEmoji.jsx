import React, { Component } from "react";
class FormEmoji extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: "",
    };
  }

  onHandleChange = (e) => {
    this.setState({ inputText: e.target.value });
    this.props.handleChange(e.target.value);
  };

  render() {
    return (
      <form>
        <input
          style={{
            height: "3rem",
            width: "100%",
            background: "white",
            border: "1px solid black",
            marginBottom: "1rem",
          }}
          type="text"
          value={this.state.inputText}
          onChange={this.onHandleChange}
        ></input>
      </form>
    );
  }
}

export default FormEmoji;
