import React, { Component } from "react";
class Emoji extends Component {
  render() {
    const { img, title } = this.props;
    return (
      <div style={{ display: "flex" }}>
        <div>{img}</div>
        <p>{title}</p>
      </div>
    );
  }
}

export default Emoji;
