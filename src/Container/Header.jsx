import React, { Component } from "react";
import Emoji from "../Components/Emoji";
import FormEmoji from "../Components/FormEmoji";
import jsonData from "../jsondata.json";

class Header extends Component {
  constructor() {
    super();
    this.state = {
      data: jsonData,
      filterData: jsonData,
    };
  }

  handleChange = (value) => {
    if (value === "") {
      this.setState({ filterData: [...this.state.data] });
    } else {
      let newArr = this.state.data.filter((obj) =>
        obj.title.toLowerCase().includes(value)
      );
      this.setState({ filterData: [...newArr, this.state.filterData] });
    }
  };

  render() {
    return (
      <div>
        <h1>Emoji Search</h1>
        <FormEmoji handleChange={this.handleChange} />
        {this.state.filterData.map(({ title, symbol }) => (
          <Emoji title={title} img={symbol}></Emoji>
        ))}
      </div>
    );
  }
}

export default Header;
