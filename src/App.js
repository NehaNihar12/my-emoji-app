import React, { Component } from "react";
import "./App.css";
import Header from "./Container/Header";

class App extends Component {
  state = {};
  render() {
    return (
      <div>
        <Header />
      </div>
    );
  }
}

export default App;
